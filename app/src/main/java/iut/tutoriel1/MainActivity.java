package iut.tutoriel1;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toast.makeText(getApplicationContext(), "Hello World !!!!", Toast.LENGTH_SHORT).show();
        Log.i("MESSAGE_A_LANOIX", "Hello World !!!!");
        final TextView numView = findViewById(R.id.numberView);
        final SeekBar bar = findViewById(R.id.seekBar);
        bar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                numView.setText(String.valueOf(progress + 1));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        final EditText crypt = findViewById(R.id.editTextCrypt);
        final EditText decrypt = findViewById(R.id.editTextDecrypt);
        final Button btnCrypt = findViewById(R.id.buttonCrypt);
        final Button btnSwap = findViewById(R.id.buttonSwap);
        btnSwap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (decrypt.isEnabled()) {
                    crypt.setEnabled(true);
                    decrypt.setEnabled(false);
                    btnCrypt.setText(R.string.decrypt);
                } else {
                    crypt.setEnabled(false);
                    decrypt.setEnabled(true);
                    btnCrypt.setText(R.string.crypt);
                }
            }
        });
        btnCrypt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Editable text = decrypt.isEnabled() ? decrypt.getText(): crypt.getText();
                String forWhat = decrypt.isEnabled() ? getString(R.string.crypt) : getString(R.string.decrypt);
                if (text.length() > 0)
                    if (decrypt.isEnabled())
                        crypt.setText(ChiffreCesar.chiffrement(text.toString(), bar.getProgress() + 1));
                    else
                        decrypt.setText(ChiffreCesar.dechiffrement(text.toString(), bar.getProgress() + 1));
                else
                    Toast.makeText(getApplicationContext(), String.format("Pas de valeur pour %s !", forWhat), Toast.LENGTH_SHORT).show();
            }
        });
        Button btnClear = findViewById(R.id.buttonClear);
        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bar.setProgress(0);
                numView.setText(R.string.baseNum);
                crypt.setText("");
                decrypt.setText("");
            }
        });
    }
}
